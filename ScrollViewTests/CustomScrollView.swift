//
//  CustomScrollView.swift
//  ScrollViewTests
//
//  Created by Victor Magalhaes on 23/07/2018.
//  Copyright © 2018 Victor Magalhaes. All rights reserved.
//

import Foundation
import UIKit

enum ViewToAnimate {
    case current, next
}

enum Orientation {
    case vertical, horizontal
}

class CustomScrollView: UIScrollView {
    
    private lazy var viewControllers = [BaseAnimatedViewController]()
    private lazy var size = CGSize()
    private lazy var controller = UIViewController()
    private lazy var viewToAnimate = ViewToAnimate.current
    private lazy var orientation = Orientation.horizontal
    
    init(controller:UIViewController, viewControllers: [BaseAnimatedViewController], size: CGSize, viewToAnimate: ViewToAnimate = .current, orientation: Orientation = .horizontal) {
        super.init(frame: .zero)
        self.viewControllers = viewControllers
        self.size = size
        self.controller = controller
        self.viewToAnimate = viewToAnimate
        self.orientation = orientation
        
        setupParameters()
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setupParameters() {
        delegate = self
        isPagingEnabled = true
        bounces = false
        translatesAutoresizingMaskIntoConstraints = false
        showsVerticalScrollIndicator = false
        contentSize = size
    }
    
    private func setupViews() {
        viewControllers.forEach { addViewController($0) }
    }
    
    private func addViewController(_ viewController: UIViewController) {
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(viewController.view)
        controller.addChildViewController(viewController)
        viewController.didMove(toParentViewController: controller)
    }
    
    private func setupConstraints() {
        for (pos, viewController) in viewControllers.enumerated() {
            addViewController(viewController)
//            let multiplier = CGFloat(1.0)
            //                pos == 0 ? CGFloat(0.8) : CGFloat(1.0)
            let anchor = pos == 0 ? topAnchor : viewControllers[pos-1].view.bottomAnchor
            NSLayoutConstraint.activate([
                viewController.view.heightAnchor.constraint(equalToConstant: size.height/CGFloat(viewControllers.count)),
                viewController.view.widthAnchor.constraint(equalToConstant: size.width),
                viewController.view.topAnchor.constraint(greaterThanOrEqualTo: anchor)
                ])
        }
    }
    
    private func currentPos() -> Int {
        return Int(contentOffset.y/(size.height/CGFloat(viewControllers.count)))
    }
    
}

extension CustomScrollView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if contentOffset.y < 0 { setContentOffset(.zero, animated: false) }
        guard (currentPos() + 1) < viewControllers.count else { return }
        animate(viewToAnimate, to: animationStep())
    }
    
    private func animate(_ view: ViewToAnimate , to step: CGFloat) {
        view == .current ?
            viewControllers[currentPos()].animate(factor: step) :
            viewControllers[currentPos()+1].animate(factor: step)
    }
    
    private func animationStep() -> CGFloat {
        let height = size.height / CGFloat(viewControllers.count)
        let step = (contentOffset.y - (CGFloat(currentPos()) * height))/height
        return step
    }
    
}
