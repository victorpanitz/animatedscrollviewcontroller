//
//  vc3.swift
//  ScrollViewTests
//
//  Created by Victor Magalhaes on 17/07/2018.
//  Copyright © 2018 Victor Magalhaes. All rights reserved.
//

import UIKit

class ViewController3: BaseAnimatedViewController {
    
    let label: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Heavy", size: 20)
        label.textAlignment = .center
        label.textColor = .white
        label.alpha = 0.3
        label.text = "TESTING ANIMATION"
        return label
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(label)
        label.frame = CGRect(x: 0, y: 200, width: view.frame.width, height: 40)
        view.backgroundColor = .red
    }
    
    override func animate(factor: CGFloat) {
        print("ANIMATING 3 WITH FACTOR:\(factor)")
        let scale = 1+(factor * 0.5)
        label.alpha = 0.3 + factor
        label.transform = CGAffineTransform(scaleX: scale, y: scale)
            .concatenating(CGAffineTransform(translationX: 0, y: 200*factor ))
    }

}
