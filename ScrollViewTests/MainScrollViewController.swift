//
//  ViewController.swift
//  ScrollViewTests
//
//  Created by Victor Magalhaes on 17/07/2018.
//  Copyright © 2018 Victor Magalhaes. All rights reserved.
//

import UIKit

class MainScrollViewController: UIViewController {

    var scrollView: CustomScrollView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView = CustomScrollView(
            controller: self,
            viewControllers: [ViewController1(), ViewController2(), ViewController3()],
            size: CGSize(width: view.frame.width, height: view.frame.height * 3),
            viewToAnimate: .next)
        
        self.view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        setupConstraints()
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate([
            scrollView.heightAnchor.constraint(equalToConstant: view.frame.height),
            scrollView.widthAnchor.constraint(equalToConstant: view.frame.width),
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}
