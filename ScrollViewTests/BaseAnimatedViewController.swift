//
//  AnimatedViewController.swift
//  ScrollViewTests
//
//  Created by Victor Magalhaes on 17/07/2018.
//  Copyright © 2018 Victor Magalhaes. All rights reserved.
//

import Foundation
import UIKit

class BaseAnimatedViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func animate(factor: CGFloat) {}
}
